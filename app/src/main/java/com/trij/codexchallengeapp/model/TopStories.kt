package com.trij.codexchallengeapp.model

data class TopStories (
    val id: Int,
    val title: String,
    val descendants: String,
    val score: Int
)