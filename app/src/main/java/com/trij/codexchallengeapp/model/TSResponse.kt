package com.trij.codexchallengeapp.model

data class TSResponse(
    val result: List<TopStories>
)