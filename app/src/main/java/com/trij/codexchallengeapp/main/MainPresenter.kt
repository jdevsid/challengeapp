package com.trij.codexchallengeapp.main

import com.google.gson.Gson
import com.trij.codexchallengeapp.model.TSResponse
import com.trij.codexchallengeapp.model.TopStories
import com.trij.codexchallengeapp.network.ApiConnector
import com.trij.codexchallengeapp.network.DBApi
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class MainPresenter (
    private val view:MainView,
    private val apiRepository:ApiConnector,
    private val gson:Gson
){
    fun getDataList(){
        doAsync {
            val data = gson.fromJson(apiRepository.request(DBApi.getData()), TSResponse::class.java)
            uiThread {
                view.showDataList(data.result)
            }
        }
    }
}