package com.trij.codexchallengeapp.main

import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.trij.codexchallengeapp.R
import com.trij.codexchallengeapp.model.TopStories
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick

class MainAdapter (
    private val result: List<TopStories>,
    private val listener: (TopStories) -> Unit)
    : RecyclerView.Adapter<TopStoriesViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopStoriesViewHolder {
        return TopStoriesViewHolder(TopStoriesUI().createView(AnkoContext.create(parent.context, parent)))
    }

    override fun getItemCount(): Int = result.size

    override fun onBindViewHolder(holder: TopStoriesViewHolder, position: Int) {
        holder.bindItem(result[position], listener)
    }

}

class TopStoriesUI : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            linearLayout {
                lparams(width = matchParent, height = wrapContent)
                padding = dip(5)
                orientation = LinearLayout.VERTICAL

                textView {
                    id = R.id.title_data
                    textSize = 16f
                }.lparams { margin = dip(6) }
                textView {
                    id = R.id.descendants_data
                    textSize = 12f
                }.lparams { margin = dip(6) }
                textView {
                    id = R.id.score_data
                    textSize = 12f
                }.lparams { margin = dip(6) }
            }
        }
    }
}

class TopStoriesViewHolder(view: View) :RecyclerView.ViewHolder(view){
    private val titleData: TextView = view.find(title_data)
    private val descendantsData: TextView = view.find(descendants_data)
    private val scoreData: TextView = view.find(score_data)
    fun bindItem(topStories: TopStories, listener: (TopStories) -> Unit){
        titleData.text = topStories.title
        descendantsData.text = topStories.descendants.toString()
        scoreData.text = topStories.score.toString()

        titleData.onClick {
            listener(topStories)
        }

    }
}