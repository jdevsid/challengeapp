package com.trij.codexchallengeapp.main

import com.trij.codexchallengeapp.model.TopStories

interface MainView{
    fun showDataList(data:List<TopStories>)
}