package com.trij.codexchallengeapp.network
import java.net.URL

class ApiConnector {
    fun request(url:String): String {
        return URL(url).readText()
    }
}